function checkValidIPAddress(ip){
  var ips = ip.split(".");
  var ms = document.getElementById("message");
  for(var i=0; i<4; i++){
    if(!isNaN(ips[i]) && parseInt(ips[i]) >= 0 && parseInt(ips[i]) <= 255){
      ms.setAttribute("hidden", "hidden");
      continue;
    } else {
      ms.innerHTML="Please provide a valid IP address.";
      ms.removeAttribute("hidden");
      return false;
    }
  }
  return true;
}

function checkedClass(){
  var aclass = document.getElementById("a");
  var bclass = document.getElementById("b");
  var cclass = document.getElementById("c");
  var ms = document.getElementById("messagec");
    if(aclass.checked==true || bclass.checked==true || cclass.checked==true){
      ms.setAttribute("hidden", "hidden");
      return true;
    } else {
      ms.innerHTML="Please selecting Network class.";
      ms.removeAttribute("hidden");
      return false;
    }
}

function showSubnetMask(subnet) {
  for (row = 32; row >= 1; row--) {
    if(subnet == allSubnets[row-1][0]){
      var subnetms = allSubnets[row-1][1].split(' ');
      var subnetm = subnetms[0];
      return subnetm;
    }  
  }
}

function getIPClass(subnet){
  var ipclass = "";
  if(subnet >= 24){
    ipclass = "C";
  } else if(subnet >= 16){
    ipclass = "B";
  } else if(subnet >= 8){
    ipclass = "A";
  } 
  return ipclass;
}

function binSub(subnetmark){
  var subs = subnetmark.split(".");
  var ind = 0;
  var bin0 = "";
  var bin = "";
  for(i = 0; i < 4; i++){
    ind = parseInt(subs[i]);
    for(j = 0; j < 8; j++){
      bin0+=ind%2;
      ind=ind/2 >> 0; //Math.floor
    }
    for(j = 7; j >= 0; j--){
      bin+=bin0[j];
    }
    if(i < 3) bin+=".";
    bin0 = "";
  }
  return bin;
}

/*function binSub(subnet){
  var bin="";
  for(i = 0;i < 4; i++){
    for(j=0; j < 8; j++){
      if(subnet != 0){
      bin+=1;
      subnet--;
      } else {
        bin+=0;
      }
    }
    if(i < 3) bin+=".";
  }
  return bin;
}*/

function wildcard(subnetmark){
  var subs = subnetmark.split(".");
  var wild="";
  for(i = 0; i < 4; i++){
    wild+=(255-subs[i]);
    if(i < 3) wild+=".";
  }
  return wild;
}

function totalNumberOfHosts(cidr){
  return 2**(32-cidr);
}

function numOfUsable(total){
  if(total<2) return 0;
  return total-2;
}

function ipType(ip){
  var subs = ip.split(".");
  if((subs[0] == 192 && subs[1] == 168)) {
    return "Private";
  } else if((subs[0] == 172 && subs[1] == 16) || (subs[0] == 172 && subs[1] ==31)) {
    return "Private";
  } else if(subs[0] == 10) {
    return "Private";
  } 
  return "Public";
}

function broadcastAd(netw,wild){
  var netw = netw.split(".");
  var wilds = wild.split(".");
  var brcast = "";
  for(i = 0; i < 4; i++){
    brcast+=(parseInt(netw[i])+parseInt(wilds[i]));
    if(i < 3) brcast+=".";
  }
  return brcast;
}

function hostIPR(netw,brc){
  var netws = netw.split(".");
  var brcs = brc.split(".");
  var ipRange = "";
  if(parseInt(netws[3])+1 > parseInt(brcs[3])-1) return "NA";
  for(i = 0; i < 3; i++){
    ipRange+=(netws[i]);
    ipRange+=".";
  }
  ipRange+=(parseInt(netws[3])+1);
  ipRange+=" - ";
  for(i = 0; i < 3; i++){
    ipRange+=(brcs[i]);
    ipRange+=".";
  }
  ipRange+=(parseInt(brcs[3])-1);
  return ipRange;
}

function netwAd(bip,bsb){
  var ips = [0,0,0,0];
  ips[0]=bip.substring(0,8);
  ips[1]=bip.substring(8,16);
  ips[2]=bip.substring(16,24);
  ips[3]=bip.substring(24,32);
  // return ips[0]+"."+ips[1]+"."+ips[2]+"."+ips[3];
  var subs = [0,0,0,0];
  subs[0]=bsb.substring(0,8);
  subs[1]=bsb.substring(9,17);
  subs[2]=bsb.substring(18,26);
  subs[3]=bsb.substring(27,35);
  // return subs[0]+"."+subs[1]+"."+subs[2]+"."+subs[3];
  var netw = "";
  var s = "";
  var snt = [0,0,0,0];
  for(i = 0; i < 4; i++){
    for(j = 0; j < 8; j++){
      s+=(ips[i][j] & subs[i][j]);
    }
    snt[i]=s;
    s="";
  }
  // return snt[0]+"."+snt[1]+"."+snt[2]+"."+snt[3];
  netw+=(integerr(snt[0])+"."+integerr(snt[1])+"."+integerr(snt[2])+"."+integerr(snt[3]));
  return netw;
}

function binary(ip){
  var ips = ip.split(".");
  var ind = 0;
  var bin0 = "";
  var bin = "";
  for(i = 0; i < 4; i++){
    ind = parseInt(ips[i]);
    for(j = 0; j < 8; j++){
      bin0+=ind%2;
      ind=ind/2 >> 0; //Math.floor
    }
    for(j = 7; j >= 0; j--){
      bin+=bin0[j];
    }
    bin0 = "";
  }
  return bin;
}

function integerr(bin){
  var inte=0;
  var n = bin.length;
  for(i = 0; i < n; i++){
    inte+=(parseInt(bin[i])*(2**(n-1-i)));
  }
  return inte;
}

function hex(inte){
  var hex0 = "";
  var hex = "0x";
  while(true){
    if(inte <= 0) break;
    if(inte%16 == 10){
      hex0+="a";
    } else if(inte%16 == 11){
      hex0+="b";
    } else if(inte%16 == 12){
      hex0+="c";
    } else if(inte%16 == 13){
      hex0+="d";
    } else if(inte%16 == 14){
      hex0+="e";
    } else if(inte%16 == 15){
      hex0+="f";
    } else {
      hex0+=inte%16;
    }
    inte=inte/16 >> 0;
  }
  for(j = hex0.length-1; j >= 0; j--){
    hex+=hex0[j];
  }
  return hex;
}

function num(subnet){
  var num = 0;
  if(subnet > 24){
    num=2**(subnet-24);
  } else if(subnet > 16){
    num=2**(subnet-16);
  } else if(subnet > 8){
    num=2**(subnet-8);
  } 
  return num;
}

function ipe(ip, subnet){
  var ips = ip.split(".");
  var ipe = "";
  if(subnet > 24){
    ipe="for " + ips[0]+"."+ips[1]+"."+ips[2]+".*";
  } else if(subnet > 16){
    ipe="for " + ips[0]+"."+ips[1]+".*.*";
  } else if(subnet > 8){
    ipe="for " + ips[0]+".*.*.*";
  } 
  return ipe;
}

// function showTitle(ip, subnet){
//   var ips = ip.split(".");
//   var num = 0;
//   var ipe = "";
//   if(subnet > 24){
//     num=2**(subnet-24);
//     ipe="for " + ips[0]+"."+ips[1]+"."+ips[2]+".*";
//   } else if(subnet > 16){
//     num=2**(subnet-16);
//     ipe="for " + ips[0]+"."+ips[1]+".*.*";
//   } else if(subnet > 8){
//     num=2**(subnet-8);
//     ipe="for " + ips[0]+".*.*.*";
//   } else if(subnet > 1) {
//     num=2**(subnet-1);
//   }
//   return "All "+ num + " of the Possible /" + subnet + " Networks " + ipe;
// }

function showPos(x, subnet, n, table, ipe){
  var nt=0;
  var br=0;
  if(subnet > 24){
    for (i = 0; i < n; i++) {
      if(x>n) break;
      br=nt+(256/n)-1;
      var row = table.insertRow(x+i);
      row.insertCell(0).innerHTML=ipe.substring(4, ipe.length-1)+nt; 
      row.insertCell(1).innerHTML=ipe.substring(4, ipe.length-1)+(nt+1)+" - "+ipe.substring(4, ipe.length-1)+(br-1);
      row.insertCell(2).innerHTML=ipe.substring(4, ipe.length-1)+br; 
      nt+=(256/n);
    }
  } else if(subnet > 16){
    for (i = 0; i < n; i++) {
      if(x>n) break;
      br=nt+(256/n)-1;
      var row = table.insertRow(x+i);
      row.insertCell(0).innerHTML=ipe.substring(4, ipe.length-3)+nt+".0"; 
      row.insertCell(1).innerHTML=ipe.substring(4, ipe.length-3)+nt+".1 - "+ipe.substring(4, ipe.length-3)+br+".254";
      row.insertCell(2).innerHTML=ipe.substring(4, ipe.length-3)+br+".255"; 
      nt+=(256/n);
    }
  } else if(subnet > 8){
    for (i = 0; i < n; i++) {
      if(x>n) break;
      br=nt+(256/n)-1;
      var row = table.insertRow(x+i);
      row.insertCell(0).innerHTML=ipe.substring(4, ipe.length-5)+nt+".0.0"; 
      row.insertCell(1).innerHTML=ipe.substring(4, ipe.length-5)+nt+".0.1 - "+ipe.substring(4, ipe.length-5)+br+".255.254";
      row.insertCell(2).innerHTML=ipe.substring(4, ipe.length-5)+br+".255.255"; 
      nt+=(256/n);
    }
  }
}